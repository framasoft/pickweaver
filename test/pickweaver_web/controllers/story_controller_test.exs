defmodule PickweaverWeb.StoryControllerTest do
  use PickweaverWeb.ConnCase
  import Pickweaver.Factory

  alias Pickweaver.Stories
  alias Pickweaver.Stories.Story

  @create_attrs %{title: "some title", description: "My description", account_id: 1, elements: []}
  @update_attrs %{title: "some updated title", description: "My updated description", elements: []}
  @invalid_attrs %{title: nil, elements: nil}

  def fixture(:story) do
    {:ok, story} = Stories.create_story(@create_attrs)
    story
  end

  setup %{conn: conn} do
    user = insert(:account)
    {:ok, conn: conn, user: user}
  end

  describe "index" do
    test "lists all stories", %{conn: conn, user: user} do
      conn = get conn, story_path(conn, :index_public_for_user, user.slug)
      assert json_response(conn, 200)["stories"] == []
    end
  end

  describe "create story" do
    test "renders story when data is valid", %{conn: conn, user: user} do
      conn = auth_conn(conn, user)
      conn = post conn, story_path(conn, :create), story: @create_attrs
      assert %{"slug" => slug} = json_response(conn, 201)["story"]

      # Show path is inaccessible
      conn = get conn, story_path(conn, :show, user.slug, slug)
      assert "Page not found" == json_response(conn, 404)["error"]
    end

    test "renders errors when data is invalid", %{conn: conn, user: user} do
      conn = auth_conn(conn, user)
      conn = post conn, story_path(conn, :create), story: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update story" do
    setup [:create_story]

    test "renders story when data is valid", %{conn: conn, story: %Story{slug: slug} = story} do
      user = story.account
      conn = auth_conn(conn, user)
      conn = patch conn, story_path(conn, :update, user.slug, slug), story: @update_attrs
      res = json_response(conn, 200)
      assert %{"slug" => ^slug} = res["story"]

      # Show path is inaccessible
      conn = get conn, story_path(conn, :show, user.slug, slug)
      assert json_response(conn, 404)["error"]
    end

    @tag :pending
    test "renders errors when data is invalid", %{conn: conn, story: %Story{slug: slug} = story, user: user} do
      conn = auth_conn(conn, story.account)
      conn = patch conn, story_path(conn, :update, user.slug, slug), story: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end

    test "renders story public where published is set", %{conn: conn, story: %Story{slug: slug} = story} do
      user = story.account
      conn = auth_conn(conn, user)
      conn = patch conn, story_path(conn, :update, user.slug, slug), story: %{published: true}
      res = json_response(conn, 200)
      assert %{"slug" => ^slug} = res["story"]

      conn = get conn, story_path(conn, :show, user.slug, slug)
      assert %{
               "published" => true
             } = json_response(conn, 200)["story"]
    end
  end

  describe "delete story" do
    setup [:create_story]

    test "deletes chosen story", %{conn: conn, story: story} do
      user = story.account
      conn = auth_conn(conn, story.account)
      conn = delete conn, story_path(conn, :delete, user.slug, story.slug)
      assert response(conn, 204)
      conn = get conn, story_path(conn, :show, user.slug, story.slug)
      assert response(conn, 404)
    end
  end

  describe "search own stories" do
    setup [:create_story]

    test "search for own story with valid search input", %{conn: conn, story: story} do
      conn = auth_conn(conn, story.account)
      title = story.title
      conn = get conn, story_path(conn, :index_own_stories), %{"search" => title}
      res = json_response(conn, 200)
      assert %{"title" => ^title} = List.first(res["stories"])
    end

    test "search for own story with invalid search input", %{conn: conn, story: story} do
      conn = auth_conn(conn, story.account)
      conn = get conn, story_path(conn, :index_own_stories), %{"search" => "toto"}
      res = json_response(conn, 200)
      assert [] = res["stories"]
    end
  end

  describe "search stories" do
    setup [:create_story]

    test "search for story with valid search input", %{conn: conn, story: story} do
      conn = auth_conn(conn, story.account)
      title = story.title
      user = story.account
      conn = patch conn, story_path(conn, :update, user.slug, story.slug), story: %{published: true}
      assert response(conn, 200)
      conn = get conn, story_path(conn, :search), %{"search" => title}
      res = json_response(conn, 200)
      assert %{"title" => ^title} = List.first(res["stories"])
    end

    test "search for story with invalid search input", %{conn: conn, story: story} do
      conn = auth_conn(conn, story.account)
      user = story.account
      conn = patch conn, story_path(conn, :update, user.slug, story.slug), story: %{published: true}
      assert response(conn, 200)
      conn = get conn, story_path(conn, :search), %{"search" => "toto"}
      res = json_response(conn, 200)
      assert [] = res["stories"]
    end
  end

  defp create_story(_) do
    story = insert(:story)
    {:ok, story: story}
  end

  defp auth_conn(conn, %Pickweaver.Accounts.Account{} = user) do
    {:ok, token, _claims} = PickweaverWeb.Guardian.encode_and_sign(user)
    conn
    |> put_req_header("authorization", "Bearer #{token}")
    |> put_req_header("accept", "application/json")
  end
end
