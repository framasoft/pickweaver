defmodule Pickweaver.Services.StorifyTest do
  @moduledoc """
  Test the `Pickweaver.Services.Import.Storify` module
  """

  use Pickweaver.DataCase
  alias Pickweaver.Services.Import.Storify
  alias Pickweaver.Services.Parser.Tweet
  use ExVCR.Mock, adapter: ExVCR.Adapter.Httpc
  import Pickweaver.Factory

  setup_all do
    ExVCR.Config.filter_url_params(true)
    ExVCR.Config.filter_sensitive_data("oauth_signature=[^\"]+", "<REMOVED>")
    ExVCR.Config.filter_sensitive_data("guest_id=.+;", "<REMOVED>")
    ExVCR.Config.filter_sensitive_data("access_token\":\".+?\"", "access_token\":\"<REMOVED>\"")
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    :ok
  end

  describe "importing a story" do
    test "import a story from a Storify URL" do
      use_cassette "madeleineproject-saison-5" do
        account = insert(:account)
        story = Storify.import_from_url("https://storify.com/clarabdx/madeleineproject-saison-5", account)

        assert %{title: "#Madeleineproject SAISON 5"} = story
        assert %{description: "Production Lumento avec la Radio Nova"} = story
        assert %{elements: [%Tweet{text: "#Madeleineproject - Saison 5 - http://madeleineproject.fr/", force_url: "https://twitter.com/clarabdx/status/932549369840234497"}]}
      end
    end

    test "import a story from a Storify URL with special encoding" do
      use_cassette "réparer un livre" do
        account = insert(:account)
        story = Storify.import_from_url("https://storify.com/Mayeu/reparer-un-livre-part-1", account)

        assert %{title: "Réparer un livre - Part 1"} = story
        assert %{description: "Comment réparer un livre. Par @LuxBox"} = story
        assert %{elements: [%Tweet{text: "Comment réparer un livre. Par @LuxBox", force_url: "https://twitter.com/LuxBox/status/337117912181637120"}]}
      end
    end

    @tag :pending
    test "import stories from Storify profile" do
      stories = use_cassette "clarabdx" do
        account = insert(:account)
        Storify.import_from_account_name(self(), "clarabdx", account)
      end

      [story | stories] = stories
      assert %{title: "#Madeleineproject SAISON 5"} = story
      [story | stories] = stories
      assert %{title: "#Madeleineproject SAISON 4"} = story
      [story | _] = stories
      assert %{title: "Les recettes de Madeleine cuisinées par les internautes"} = story
    end

    test "list stories from Storify profile" do
      stories = use_cassette "clarabdx" do
        _account = insert(:account)
        Storify.list_from_account_name("clarabdx", self())
      end
      assert %{url: "https://storify.com/clarabdx/madeleineproject-saison-5"} = hd(stories)
    end

    test "import a tweet from a wrong URL" do
      invalid_storify = use_cassette "bad-url-toto" do
        account = insert(:account)
        Storify.import_from_url("toto", account)
      end

      assert {:error, :not_storify} == invalid_storify
    end
  end
end
