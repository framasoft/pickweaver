defmodule Pickweaver.ParserTestTwitter do
  use Pickweaver.DataCase

  alias Pickweaver.Services.Parser
  alias Pickweaver.Services.Parser.{Tweet, Opengraph, Youtube, Facebook}
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  setup_all do
    ExVCR.Config.filter_url_params(true)
    ExVCR.Config.filter_sensitive_data("oauth_signature=[^\"]+", "<REMOVED>")
    ExVCR.Config.filter_sensitive_data("guest_id=.+;", "<REMOVED>")
    ExVCR.Config.filter_sensitive_data("access_token\":\".+?\"", "access_token\":\"<REMOVED>\"")
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    :ok
  end

  @single_urls [
    %{
      url: "https://framablog.org/2018/03/21/peertube-beta-une-graine-dalternative-a-youtube-vient-declore/",
      type: "Opengraph",
      data: %Opengraph{
        title: "PeerTube bêta : une graine d’alternative à YouTube vient d’éclore – Framablog"
      }
    },
    %{
      url: "https://www.youtube.com/watch?v=rJGifTou5FE",
      type: "YouTube",
      data: %Youtube{
        title: "Ten Illegal Things To Do In London"
      }
    },
    %{
      url: "https://www.facebook.com/framasoft/photos/a.10151478218874615.1073741825.62346699614/10155647646844615/?type=3&theater",
      type: "Facebook",
      data: %Facebook{
        title: "Framasoft - Publications",
        description: "ToS;DR - répondre au plus grand mensonge d'Internet\nhttps://framablog.org/2018/03/27/tosdr-repondre-au-plus-grand-mensonge-dinternet/"
      }
    }
  ]

  describe "parser" do
    use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

    # Process multiple single_urls
    Enum.each(
      @single_urls,
      fn url ->
        @url url

        test "process_single_url/1 with valid url from #{url.type}" do
          data = @url.data
          res = use_cassette "single-url-#{@url.url}" do
            Parser.process_single_url(@url.url)
          end
          assert data = res
        end
      end
    )
  end
end
