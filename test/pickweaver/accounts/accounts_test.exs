defmodule Pickweaver.AccountsTest do
  use Pickweaver.DataCase
  use Bamboo.Test
  import Pickweaver.Factory

  alias Pickweaver.Accounts

  describe "accounts" do
    alias Pickweaver.Accounts.Account

    @valid_attrs %{email: "foo@bar.tld", password: "some password", role: 42, username: "my account"}
    @update_attrs %{email: "foo@fighters.tld", password: "some updated password", role: 43, username: "some updated user account"}
    @invalid_attrs %{email: nil, password: nil, role: nil, username: nil}

    def account_fixture(attrs \\ %{}) do
      {:ok, account} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_account()

      %{account | password: nil}
    end

    test "list_accounts/0 returns all accounts" do
      account = account_fixture()
      assert Accounts.list_accounts() == [account]
    end

    test "get_account!/1 returns the account with given id" do
      account = account_fixture()
      assert Accounts.get_account!(account.id) == account
    end

    test "create_account/1 with valid data creates a account" do
      assert {:ok, %Account{} = account} = Accounts.create_account(@valid_attrs)
      assert %Bamboo.Email{} = Pickweaver.Accounts.Service.Activation.send_confirmation_email(account)
      assert account.email == "foo@bar.tld"
      assert account.role == 42
      assert account.username == "my account"
      assert Date.compare(account.confirmation_sent_at, DateTime.utc_now()) == :eq

      assert_delivered_email Pickweaver.Email.Account.confirmation_email(account)
    end

    test "create_account/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_account(@invalid_attrs)
    end

    test "validate_account" do
      account = account_fixture()
      {:ok, %Account{} = account_validated} = Pickweaver.Accounts.Service.Activation.check_confirmation_token(account.confirmation_token)
      assert Date.compare(account_validated.confirmed_at, DateTime.utc_now()) == :eq
      assert account_validated.confirmation_token == nil
      assert account_validated.confirmation_sent_at == nil
    end

    test "update_account/2 with valid data updates the account" do
      account = account_fixture()
      assert {:ok, account} = Accounts.update_account(account, @update_attrs)
      assert %Account{} = account
      assert account.email == "foo@fighters.tld"
      assert account.role == 43
      assert account.username == "some updated user account"
    end

    test "update_account/2 with invalid data returns error changeset" do
      account = account_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_account(account, @invalid_attrs)
      assert account == Accounts.get_account!(account.id)
    end

    test "get_account_by_slug/1 with valid data gets the account" do
      account = account_fixture()
      assert {:ok, %Account{} = account} == Accounts.get_account_by_slug(account.slug)
    end

    test "get_account_by_slug/1 with slug for unexistant account returns error" do
      assert {:error, :not_found} == Accounts.get_account_by_slug("invalid slug")
    end

    test "find_by_email/1 with valid data gets the account" do
      account = account_fixture()
      assert {:ok, account} == Accounts.find_by_email(account.email)
    end

    test "find_by_email/1 with slug for unexistant account returns nil" do
      assert {:error, :not_found} == Accounts.find_by_email("invalid email")
    end

    test "find_by_username/1 with valid data gets the account" do
      account = account_fixture()
      assert {:ok, account} == Accounts.find_by_username(account.username)
    end

    test "find_by_username/1 with slug for unexistant account returns nil" do
      assert {:error, :not_found} == Accounts.find_by_username("invalid username")
    end

    test "authenticate/1 with valid credentials" do
      account = account_fixture()
      assert {:ok, _, _} = Accounts.authenticate(%{account: account, password: @valid_attrs.password})
    end

    test "authenticate/1 with invalid credentials" do
      account = account_fixture()
      assert {:error, :unauthorized} = Accounts.authenticate(%{account: account, password: "bad password"})
    end

    test "delete_account/1 deletes the account" do
      account = account_fixture()
      assert {:ok, %Account{}} = Accounts.delete_account(account)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_account!(account.id) end
    end

    test "change_account/1 returns a account changeset" do
      account = account_fixture()
      assert %Ecto.Changeset{} = Accounts.change_account(account)
    end
  end

  describe "service tokens" do
    setup do
      account = insert(:account)
      {:ok, account: account}
    end

    @token %{"service" => "twitter", "data" => %{"access_token" => %{"oauth_token" => "foo", "oauth_token_secret" => "bar"}}}
    @token_updated %{"service" => "twitter", "data" => %{"access_token" => %{"oauth_token" => "foobar", "oauth_token_secret" => "foofoo"}}}

    test "create a token", %{account: account} do
      token_attrs = Map.put(@token, "account_id", account.id)
      assert {:ok, _token} = Accounts.create_token(token_attrs)
    end

    test "update a token", %{account: account} do
      token_attrs = Map.put(@token, "account_id", account.id)
      {:ok, _token} = Accounts.create_token(token_attrs)
      {:ok, token} = Accounts.get_token(account, "twitter")
      token_attrs = Map.put(@token_updated, "account_id", account.id)
      {:ok, token_updated} = Accounts.create_token(token_attrs)
      token_id = token.id
      assert token_id == token_updated.id
    end
  end
end
