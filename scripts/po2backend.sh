#!/bin/bash

for i in po/*/LC_MESSAGES/*.po
do
    j=$(echo $i | cut -d '.' -f 1 | cut -d '/' -f 2)
    mv $i priv/gettext/$j/LC_MESSAGES/
done
