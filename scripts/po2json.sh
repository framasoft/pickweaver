#!/bin/bash
po2json -i po/en/LC_MESSAGES/pickweaver.po -t js/src/i18n/en.json --progress none -o po/default.json

for i in po/*/LC_MESSAGES/pickweaver.po
do
    j=$(echo $i | cut -d '.' -f 1 | cut -d '/' -f 2)
    po2json -i $i -t po/default.json --progress none | scripts/renest_json.pl > po/$j.json
    mv po/$j.json js/src/i18n/
    rm $i
done
rm po/default.json
