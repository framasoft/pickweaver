defmodule Pickweaver.Repo.Migrations.CreateFiles do
  use Ecto.Migration

  def change do
    create table(:files) do
      add :uuid, :string
      add :path, :string
      add :name, :string
      add :account_id, references(:accounts)

      timestamps()
    end

    create unique_index(:files, [:uuid])

  end
end
