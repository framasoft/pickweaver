defmodule Pickweaver.Repo.Migrations.AddMediaToStories do
  use Ecto.Migration

  def change do
    alter table("stories") do
      add :media_id, references(:files)
    end
  end
end
