defmodule Pickweaver.Repo.Migrations.AddPublishedDatetimeToStories do
  use Ecto.Migration

  def change do
    alter table("stories") do
      add :published_at, :utc_datetime, null: true
    end
  end
end
