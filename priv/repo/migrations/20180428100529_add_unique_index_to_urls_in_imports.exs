defmodule Pickweaver.Repo.Migrations.AddUniqueIndexToUrlsInImports do
  use Ecto.Migration

  def up do
    create unique_index("imports", [:url, :account_id], [name: "index_unique_imports_url_account_id"])
  end

  def down do
    drop index("imports", [:url, :account_id], name: "index_unique_imports_url_account_id")
  end
end
