defmodule Pickweaver.Repo.Migrations.AddDisplayNameAndAvatarToAccounts do
  use Ecto.Migration

  def change do
    alter table("accounts") do
      add :display_name, :string
      add :avatar, :string
    end
  end
end
