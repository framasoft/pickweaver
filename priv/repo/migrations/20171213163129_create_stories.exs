defmodule Pickweaver.Repo.Migrations.CreateStories do
  use Ecto.Migration

  def change do
    create table(:stories) do
      add :title, :string
      add :description, :string
      add :body, :text
      add :elements, :text
      add :tag_id, references(:tags)
      add :account_id, references(:accounts)

      timestamps()
    end
  end
end
