defmodule Pickweaver.Repo.Migrations.IncreaseStoryHeaderUrlLength do
  use Ecto.Migration

  def up do
    alter table("stories") do
      modify :header, :string, size: 2048
    end
  end

  def down do
    alter table("stories") do
      modify :header, :string, size: 256
    end
  end
end
