const translations = [
  'fr_FR',
  'en_US',
];
/* eslint-enable */

const prefix = function prefix(lang) {
  const parts = lang.split('_');
  if (parts.length > 1) {
    return parts[0];
  }
  return lang;
};

const matchPrefix = function matchPrefix(lang) {
  /* eslint-disable */
  for (const translation of translations) {
    if (prefix(translation) === lang) {
      return translation;
    }
  }
  return null;
  /* eslint-enable */
};

const translate = function translate(language, languages) {
  if (translations.includes(language)) {
    return language;
  } else if (matchPrefix(language)) {
    return matchPrefix(language);
  }
  return translate(languages.shift(), languages);
};

function detectLang() {
  const languages = [];
  const nl = navigator.languages;
  for (let i = 0; i < nl.length; i += 1) {
    languages.push(nl[i]);
  }
  if (languages.length !== 0) {
    return translate(languages.shift(), languages);
  }
  console.log('Using old style language detection.');
  const userLang = navigator.language || navigator.userLanguage;
  return translate(userLang);
}

export { translations, detectLang };
