import { expect } from 'chai';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Home from '@/components/Home';
import Vuex from 'vuex';
import en from '@/i18n/en.json';

const VuexI18n = require('vuex-i18n/dist/vuex-i18n.cjs');

describe('Home.vue', () => {
  it('Correctly render home', () => {
    const msg = 'A pick is a selection of elements to tell a story';

    const localVue = createLocalVue();
    localVue.use(Vuex);
    const store = new Vuex.Store({});
    localVue.use(VuexI18n.plugin, store);

    localVue.i18n.add('en_US', en);
    localVue.i18n.set('en_US');
    localVue.i18n.fallback('en_US');

    const wrapper = shallowMount(Home, {
      localVue,
      propsData: { msg },
      store,
    });
    expect(wrapper.text()).to.include(msg);
  });
});

/*
describe('Home.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(Home);
    const vm = new Constructor().$mount();
    expect(vm.$el.querySelector('.home h4.card-title:first-child').textContent)
      .to.equal('Start by weaving a pick');
  });
});

 */
