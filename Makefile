locales:
	json2po -P -i js/src/i18n/en.json -t js/src/i18n/en.json -o pot/pickweaver.pot
	cp priv/gettext/*pot pot/

push-locales: locales
	zanata-cli -q -B push

pull-locales:
	zanata-cli -q -B pull
	scripts/po2json.sh
	scripts/po2backend.sh

stats-locales:
	zanata-cli -q stats

push-trad-to-zanata:
	scripts/push-trad-to-zanata.sh $(filter-out $@,$(MAKECMDGOALS))

add-key-locales:
	scripts/locale-add-key.pl "$(subst ",\",$(filter-out $@,$(MAKECMDGOALS)))"

# empty targets to be able to use MAKECMDGOALS as arguments to scripts
%:
	@:
