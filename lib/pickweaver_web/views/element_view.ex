defmodule PickweaverWeb.ElementView do
  use PickweaverWeb, :view
  require Logger
  alias PickweaverWeb.ElementView
  alias PickweaverWeb.{TweetView, OpenGraphView, BasicView, YoutubeView}

  def render("elements.json", %{elements: elements}) do
    render_many(elements, ElementView, "element.json")
  end

  def render("element.json", %{element: element}) do
    %{
      id: element.id,
      created_at: element.inserted_at,
      updated_at: element.updated_at,
      data: render_one(element, ElementView, "data.json"),
      type: element.type,
      url: element.url,
    }
  end

  def render("data.json", %{element: element}) do
    unless element == nil do
      case element.type do
        :tweet ->
          render_one(element.data, TweetView, "tweet.json", as: :tweet)
        :youtube ->
          render_one(element.data, YoutubeView, "youtube.json", as: :youtube)
        :opengraph ->
          render_one(element.data, OpenGraphView, "opengraph.json", as: :opengraph)
        :basic ->
          render_one(element.data, BasicView, "basic.json", as: :basic)
        :facebook ->
          render_one(element.data, OpenGraphView, "opengraph.json", as: :opengraph)
        :text ->
          element.data
      end
    end
  end
end
