defmodule PickweaverWeb.ViewHelpers do

  # TODO : these functions are depreciated and shouldn't be used anyway
  @doc """
  Turns a string indexed map to an atom indexed one
  """
  def keys_to_atoms(string_key_map) when is_map(string_key_map) do
    for {key, val} <- string_key_map, into: %{}, do: {String.to_atom(key), keys_to_atoms(val)}
  end

  def keys_to_atoms(value), do: value
end
