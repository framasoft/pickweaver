defmodule PickweaverWeb.TweetView do
  use PickweaverWeb, :view
  require Logger

  def render("tweet.json", %{tweet: tweet}) do
    %{
      type: "tweet",
      text: toLinks(tweet),
      user: process_user(tweet["user"]),
      id: tweet["id"],
      images: tweet["images"],
      videos: tweet["videos"],
      created_at: tweet["created_at"],
    }
  end

  defp process_user(%ExTwitter.Model.User{} = user) do
    user
    |> Map.from_struct()
    |> stringify_keys()
    |> process_user()
  end

  defp process_user(%{} = user) do
    %{username: user["name"], screen_name: user["screen_name"], profile: Pickweaver.MediaProxy.gen_url(user["profile_image_url"])}
  end

  defp stringify_keys(map = %{}) do
    map
    |> Enum.map(fn {k, v} -> {Atom.to_string(k), v} end)
    |> Enum.into(%{})
  end

  def toLinks(tweet) do
    urls = Regex.scan(~r/(http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?/iu, tweet["text"], [capture: :first])
    _text = urls
      |> processURLs()
      |> replaceURLs(tweet["text"])
      |> processHashtags(tweet["hashtags"])
      |> processUsers(tweet["mentions"])
  end

  defp processURLs(urls) do
    Enum.map(urls, fn url -> %{url: url, html: processURL(url)} end)
  end

  def processURL([url]) do
    "<a class=\"tweet-card-link\" href=\"" <> url <> "\">" <> url <> "</a>"
  end

  defp replaceURLs([head | tail], body) do
    body = replaceURLs(tail, body)
    body = replaceURL(head, body)
    body
  end

  defp replaceURLs([], body) do
    body
  end

  defp replaceURL(url_map, body) do
    String.replace(body, hd(url_map.url), url_map.html)
  end

  defp processHashtags(text, [head | tail]) do
    text
    |> String.replace("##{head["text"]}", "<a class=\"tweet-card-link\" href=\"https://twitter.com/hashtag/#{head["text"]}\">##{head["text"]}</a>")
    |> processHashtags(tail)
  end

  defp processHashtags(text, []) do
    text
  end

  defp processUsers(text, [head | tail]) do
    text
    |> String.replace("@#{head["screen_name"]}", "<a class=\"tweet-card-link\" href=\"https://twitter.com/#{head["screen_name"]}\">@#{head["screen_name"]}</a>")
    |> processUsers(tail)
  end

  defp processUsers(text, []) do
    text
  end

  def to_date(string_date) do
    {:ok, datetime} = Timex.parse(string_date, "{WDshort} {Mshort} {D} {h24}:{m}:{s} {Z} {YYYY}")
    {:ok, datetime_formatted} = Timex.format(datetime, "{D} {Mshort} {YYYY} {h24}:{m}:{s}")
    datetime_formatted
  end

  def to_unix(string_date) do
    {:ok, datetime} = Timex.parse(string_date, "{WDshort} {Mshort} {D} {h24}:{m}:{s} {Z} {YYYY}")
    DateTime.to_unix(datetime)
  end

  def to_struct(kind, attrs) do
    struct = struct(kind)
    Enum.reduce Map.to_list(struct), struct, fn {k, _}, acc ->
      case Map.fetch(attrs, Atom.to_string(k)) do
        {:ok, v} -> %{acc | k => v}
        :error -> acc
      end
    end
  end
end
