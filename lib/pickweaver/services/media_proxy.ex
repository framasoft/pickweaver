defmodule Pickweaver.MediaProxy do
  @base64_opts [padding: false]

  @doc """
  Generates a url for a secure endpoint
  """
  def gen_params(url) when url != nil do
    secret_key = Application.get_env(:pickweaver, PickweaverWeb.Endpoint)[:secret_key_base]
    hmac = :crypto.hmac(:sha256, secret_key, url)
           |> Base.encode16
           |> String.downcase
    {hmac, Base.encode64(url, @base64_opts)}
  end

  def gen_params(nil) do
    nil
  end

  defp check_signature(url, signature) do
    {signature, Base.encode64(url)} === gen_params(url)
  end

  @doc """
  Generates a full URL with encoded url and signature
  """
  def gen_url(url) do
    with {signature, encoded_url} <- gen_params(url) do
      PickweaverWeb.Router.Helpers.proxy_url(PickweaverWeb.Endpoint, :proxify, encoded_url, signature)
    end
  end

  @doc """
  Decodes and check that the signature corresponds to the URL
  """
  def decode_url(sig, url_encoded) do
    url = Base.url_decode64!(url_encoded, @base64_opts)
    {local_sig, _url} = gen_params(url)
    if local_sig == sig do
      {:ok, url}
    else
      {:error, :invalid_signature}
    end
  end
end
