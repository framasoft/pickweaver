defmodule Pickweaver.Services.Connect.Twitter do

  alias Pickweaver.Accounts
  alias Pickweaver.Accounts.{Token, Account}

  @doc """
  Proceed to Twitter Auth
  """
  def twitter_auth(callback) do
    token = ExTwitter.request_token(callback)
    ExTwitter.authenticate_url(token.oauth_token)
  end

  @doc """
  Callback route called when Twitter Auth succeeds
  """
  @spec twitter_auth_callback(String.t, String.t, integer) :: tuple
  def twitter_auth_callback(oauth_token, oauth_verifier, account_id) do
    with {:ok, access_token} <- ExTwitter.access_token(oauth_verifier, oauth_token),
         {:ok, %Token{} = _token} <- Accounts.create_token(%{"service" => "twitter", "data" => %{"access_token" => access_token}, "account_id" => account_id }) do
      %{status: :ok}
    else
      _ -> %{status: :error}
    end
  end

  @doc """
  Returns if we have twitter authentification tokens for the current account
  """
  def twitter_has_token(account) do
    case Accounts.get_token(account, "twitter") do
      {:ok, _} ->
        configure_ex_twitter(account)
        try do
          ExTwitter.verify_credentials
          %{status: :ok}
        rescue
          ExTwitter.Error ->
            with {:ok, %Token{} = token} <- Accounts.get_token(account, "twitter") do
              Accounts.delete_token(token) # Delete the token if it's expired
            end
            %{status: :expired}
        end
      _ ->
        %{status: :missing}
    end
  end

  @doc """
  Configures the current ex_twitter process to use it's Twitter token
  """
  @spec configure_ex_twitter(Account.t) :: String.t
  def configure_ex_twitter(%Account{} = account) do
    with {:ok, %Token{data: data}} <- Accounts.get_token(account, "twitter") do
      [consumer_key: consumer_key, consumer_secret: consumer_secret, access_token: _, access_token_secret: _] = ExTwitter.configure()
      ExTwitter.configure(
        :process,
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        access_token: data["access_token"]["oauth_token"],
        access_token_secret: data["access_token"]["oauth_token_secret"]
      )
      data["screen_name"]
    end
  end
end
