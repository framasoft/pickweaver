defprotocol Pickweaver.Services.Parse do

  @doc """
  Parses a single URL to an Element

  ## Examples

      iex> parse_url("https://mywebsite.tld")

  """
  def parse_url(data)

  @doc """
  Parses multiples URLs to Elements
  """
  def parse_urls(data)
end

defimpl Pickweaver.Services.Parse, for: Pickweaver.Services.Parser.Tweet do
  def parse_url(tweet_url), do: Pickweaver.Services.Parser.Tweet.parse_single_url(tweet_url)
  def parse_urls(tweet_urls), do: Pickweaver.Services.Parser.Tweet.parse_twitter_url_list(tweet_urls)
end


defimpl Pickweaver.Services.Parse, for: Pickweaver.Services.Parser.Youtube do
  def parse_url(youtube_url), do: Pickweaver.Services.Parser.Youtube.parse_youtube_url(youtube_url)
  def parse_urls(youtube_urls), do: Pickweaver.Services.Parser.Youtube.parse_youtube_urls(youtube_urls)
end

defimpl Pickweaver.Services.Parse, for: Pickweaver.Services.Parser.Opengraph do
  def parse_url(opengraph_url), do: Pickweaver.Services.Parser.Opengraph.parse_url(opengraph_url)
  def parse_urls(opengraph_urls), do: Pickweaver.Services.Parser.Opengraph.parse_urls(opengraph_urls)
end
