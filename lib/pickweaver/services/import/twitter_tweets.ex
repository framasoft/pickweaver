defmodule Pickweaver.Services.Import.TwitterTweets do
  @moduledoc """
  Import a Twitter and it's responses (also called Twitter Thread)
  """
  require Logger

  @doc """
  Calls the twitter API to get a tweet and it's replies from a Twitter URL. Returns a map of story params (title, elements and description)
  """
  @spec import_from_twitter_url(String.t) :: map()
  def import_from_twitter_url(url) do
    code = Regex.run(~r/http(?:s)?:\/\/(?:www.)?twitter\.com\/[a-zA-Z0-9_]+\/status\/([0-9]+)/iu, url, [capture: :all_but_first])
    if code do
      [%ExTwitter.Model.Tweet{} = tweet] = ExTwitter.lookup_status(code, [tweet_mode: "extended"])
      Logger.debug("Finding replies")
      replies = ExTwitter.search("to:#{tweet.user.screen_name} from:#{tweet.user.screen_name}", [tweet_mode: "extended", since_id: tweet.id, count: 100, result_type: "recent"])
      Logger.debug("Found replies")
      replies = Enum.sort_by(replies, fn x -> x.created_at end)
      elements = process_elements([tweet] ++ replies)
      Logger.debug("Elements processed by twitter tweet importer")
      Logger.debug(inspect elements)
      {:ok, story_params(elements, tweet)}
    else
      {:error, :not_tweet}
    end
  end

  @spec process_elements(list(ExTwitter.Model.Tweet.t)) :: list(String.t)
  defp process_elements(tweets) do
    tweets
    |> Enum.map(&Pickweaver.Services.Parser.Tweet.process_tweet/1)
    |> Pickweaver.Stories.create_elements()
    |> Enum.map(fn element -> element.id end)
  end

  @spec story_params(list(String.t), ExTwitter.Model.Tweet.t) :: map()
  defp story_params(elements, tweet) do
    title =
      case Kernel.length(elements) do
        1 ->
          "Tweet from #{tweet.user.screen_name}"
        _ ->
          "Tweets from #{tweet.user.screen_name}"
      end
    %{
      "title" => title,
      "elements" => elements,
      "description" => tweet.full_text,
    }
  end
end
