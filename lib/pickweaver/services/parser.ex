defmodule Pickweaver.Services.Parser do
  @moduledoc """
  Parser Service.

  Handles fetching information from various sources
  """
  require Logger
  alias Pickweaver.Accounts.Account
  alias Pickweaver.Services.Parser.{Tweet, Opengraph, Youtube, Facebook, Text, Element, ParserError}

  @doc """
  Parse a list of `Element` elements
  """
  @spec parse(List.t, Account.t) :: List.t
  def parse([element | tail], account) do
    [parse_element(element, account)] ++ parse(tail, account)
  end

  @doc false
  @spec parse(List.t, Account.t) :: List.t
  def parse([], _account) do
    []
  end

  @doc """
  Parses a single URL, used for direct element fetching
  """
  @spec process_single_url(String.t, Account.t) :: struct()
  def process_single_url(url, account) do
    url |> process_url() |> fetch_single_url(account)
  end

  @doc """
  Parses a single `Pickweaver.Services.Parser.Element` element
  """
  @spec parse_element(Element.t, Account.t) :: struct()
  def parse_element(%Element{} = element, %Account{} = account) do
    if Enum.member?([:text, :twitter, :facebook, :youtube, :other], element.type) do
      if element.type != :text do
        process_single_url(element.data, account)
      else
        %Text{text: cleanHTML(element.data)}
      end
    else
      detect_type(element.data, account)
    end
  end

  @doc """
  Detects type of content provided
  """
  @spec detect_type(String.t, Account.t) :: struct()
  defp detect_type(data, account) do
    parse_element(%Element{data: data, type: :text}, account)
  end

  @doc """
  Parses the URL to return it's type
  """
  @spec process_url(String.t) :: %{url: String.t, type: atom()}
  def process_url(url) do
    Logger.debug("Processing URL:")
    Logger.debug(url)
    cond do
      nil != Regex.run(~r/http(?:s)?:\/\/(?:www.)?twitter\.com\/[a-zA-Z0-9_|i\/web\/]+\/status\/([0-9]+)/iu, url, [capture: :first]) ->
        %{url: url, type: :twitter}
      nil != Regex.run(~r/^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/iu, url, [capture: :first]) ->
        %{url: url, type: :youtube}
      nil != Regex.run(~r/(?:https?:\/\/)?(?:www\.)?(mbasic.facebook|m\.facebook|facebook|fb)\.(com|me)\/(?:(?:\w\.)*#!\/)?(?:pages\/)?(?:[\w\-\.]*\/)*([\w\-\.]*)/iu, url, [capture: :first]) ->
        %{url: url, type: :facebook}
      nil != Regex.run(~r/^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/iu, url, [capture: :first]) ->
        %{url: url, type: :other}
      true ->
        %{url: nil, type: :text, data: url}
    end
  end

  @doc """
  Fetches informations on an URL and returns corresponding struct
  """
  @spec fetch_single_url(String.t, Account.t) :: struct()
  defp fetch_single_url(url_with_type, account) do
    case url_with_type.type do
      :twitter ->
        with {:ok, data} <- hd(Tweet.parse_twitter_url_list([url_with_type.url], account)) do
          data
        end
      :youtube ->
        with {:ok, data} <- Youtube.parse_youtube_url(url_with_type.url) do
          data
        end
      :facebook ->
        with {:ok, data} <- Facebook.parse_facebook_url(url_with_type.url) do
          data
        end
      :other ->
        with {:ok, data} <- process_other_url(url_with_type.url) do
          data
        end
    end
  end

  @doc """
  Process a default URL with OpenGraph, Basic parser or Image
  """
  @spec process_other_url(String.t) :: {:ok, struct()}
  defp process_other_url(url) do
    case HTTPoison.get(url, [], [ssl: [{:versions, [:'tlsv1.2']}], hackney: [follow_redirect: true]]) do
      {:ok, response} ->
        case List.keyfind(response.headers, "Content-Type", 0) do
          {"Content-Type", content_type} ->
            do_process_other_url(response, content_type, url)
          nil -> # Ugly hack to handle lowercase header
            case List.keyfind(response.headers, "content-type", 0) do
              {"content-type", content_type} ->
                do_process_other_url(response, content_type, url)
              nil ->
                Logger.error("Unknown content-type")
                Logger.error(inspect response.headers)
                {:ok, %Text{text: url, force_url: url}}
            end
        end
      {:error, _} ->
        {:ok, %Text{text: url, force_url: url}}
    end
  end

  @spec do_process_other_url(HTTPoison.Response.t, String.t, String.t) :: {:ok, any()}
  defp do_process_other_url(%HTTPoison.Response{headers: _headers} = response, content_type, url) do
    case content_type do
      "text/html" <> _ ->
        try do
          Opengraph.parse_url(url, response.body)
        rescue
          err in ParserError ->
            Logger.error(inspect err)
            {:ok, %Text{text: err, force_url: err}}
        end
      "text/plain" <> _ ->
        {:ok, %Text{text: response.body, force_url: url}}
      "image" <> _ ->
        Logger.debug("Found an image")
        {:ok, %Text{text: "<img src=\"#{url}\" alt=\"Illustration picture\" />", force_url: url}}
      _ ->
        Logger.debug("Found something unknown")
        {:ok, %Text{text: url, force_url: url}}
    end
  end

  @doc """
  Cleans HTML from an user text
  """
  @spec cleanHTML(String.t) :: String.t
  def cleanHTML(body) do
    HtmlSanitizeEx.markdown_html(body)
  end

  @spec process_multiple_urls(list(String.t), Account.t) :: struct()
  def process_multiple_urls(urls, account) do
    elements = urls
      |> Enum.map(&process_url(&1))
      |> Enum.with_index()

    return_list = Enum.map(1..length(urls), fn _ -> false end)

    existing_elements = Enum.filter(
      Enum.map(
        elements, fn {element, index} ->
          if is_nil element.url do
            {nil, index}
          else
            { Pickweaver.Stories.get_element_by_url(element.url), index }
          end
        end
      ),
      fn {res, _} -> res != nil end
    )
    elements = remove_existing_elements_from_list(elements, existing_elements)

    tweets = Enum.filter(elements, fn {element, _index} -> element.type == :twitter end)
    others = Enum.filter(elements, fn {element, _index} -> element.type == :other end)
    texts = Enum.filter(elements, fn {element, _index} -> element.type == :text end)

    return_list
    |> Tweet.parse_list_with_index(tweets, account)
    |> Text.parse_list_with_index(texts)
    |> parse_list_with_index_for_others(others)
    |> add_existing_elements(existing_elements)
    |> Enum.filter(fn elem -> elem != false end)
  end

  defp add_existing_elements(return_list, existing_elements_with_index) do
    Logger.debug("Adding Existing Elements")
    map_elements_to_positions(
      Enum.map(existing_elements_with_index,
        fn {elem, _} ->
          elem
        end
      ),
      existing_elements_with_index, return_list)
  end

  @spec remove_existing_elements_from_list(list(), list()) :: list()
  defp remove_existing_elements_from_list(list, [{_element,index} | tail]) do
    Logger.debug("Removing existing element from list")
    list
    |> remove_existing_elements_from_list(tail)
    |> remove_from_enum(index)
  end

  defp remove_existing_elements_from_list(list, []) do
    list
  end

  defp remove_from_enum(list, index_to_remove) do
    Enum.filter(list, fn {_elem, index} -> index != index_to_remove end)
  end

  @spec parse_list_with_index_for_others(list(), list()) :: list()
  defp parse_list_with_index_for_others(return_list, others) do
    Logger.debug("Parsing element list for other elements")
    other_elements = Enum.map(others, fn {element, _index} ->
      {:ok, element} = process_other_url(element.url)
      element
    end)

    map_elements_to_positions(other_elements, others, return_list)
  end

  @doc """
  Put back elements to original positions.

  * `_elements` holds the parsed element list
  * `data_list` holds the original mapping between url and index
  * `return_list` is an empty list that will be filled with elements in the correct order
  """
  @spec map_elements_to_positions(list(), list(), list()) :: list()
  def map_elements_to_positions([head|tail] = _elements, data_list, return_list) do
    Logger.debug("Mapping some elements to their position")
    case Enum.find(data_list, fn {elem, _index} -> elem.url == head.force_url end) do
      {_, index} ->
        List.replace_at(map_elements_to_positions(tail, data_list, return_list), index, head)
      nil -> # TODO : Somehow this failed, needs investigations
        map_elements_to_positions(tail, data_list, return_list)
    end
  end

  @spec map_elements_to_positions(list(), list(), list()) :: list()
  def map_elements_to_positions([], _data_list, return_list) do
    return_list
  end
end
