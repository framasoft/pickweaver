defmodule Pickweaver.Services.Parser.Tweet do
  @moduledoc """
  Represents a Tweet element.

  Twitter Parser that connects to the Twitter API to get metadata on a tweet
  """
  require Logger
  alias Pickweaver.Services.Parser.Tweet
  alias Pickweaver.Services.Parser
  alias Pickweaver.Accounts.Account
  alias Pickweaver.Services.Connect.Twitter, as: ConnectTwitter

  #@enforce_keys [:id, :user, :created_at, :text, :force_url]
  defstruct [:id, :user, :text, :created_at, :images, :videos, :force_url, :hashtags, :mentions]
  @type t :: %Tweet{id: String.t, user: map(), text: String.t, created_at: %DateTime{}, images: list(), videos: list(), force_url: String.t, hashtags: list(), mentions: list()}

  @doc """
  Maps a list of Twitter URLs to their corresponding elements

  Both `https://twitter.com/<username>/status/<tweet_id>` and `https://twitter.com/statuses/<tweet_id>` forms are supported.

  ## Examples

      iex> parse_twitter_url_list(["https://twitter.com/valentinsocha/status/957685409160597507"])
      [%Tweet{}]

  """
  @spec parse_twitter_url_list(list(String.t), Account.t) :: list(Tweet.t)
  def parse_twitter_url_list(urls, %Account{} = account) do
    Logger.debug("Parsing url list and return Tweet elements")
    urls
    |> Enum.map(fn url -> Regex.run(~r/http(?:s)?:\/\/(?:www.)?twitter\.com\/[a-zA-Z0-9_|i\/web\/]+\/status\/([0-9]+)/iu, url, [capture: :all_but_first]) end)
    |> Enum.chunk(80, 80, [])
    |> parse_twitter_codes_list(account)
  end

  @doc """
  Maps a list of Twitter URLs to their corresponding elements, while respecting order

  * `return_list` is an empty list that will be returned filled
  * `data_list` is the list of url - index mappings
  """
  @spec parse_list_with_index(list(), list({%{required(atom()) => String.t, required(atom()) => atom()}, integer()}), Account.t) :: list()
  def parse_list_with_index(return_list, data_list, account) do
    Logger.debug("Parsing element list for Twitter elements")

    # Find all elements
    elements = data_list
      |> Enum.map(fn {element, _index} -> element.url end)
      |> parse_twitter_url_list(account)
    Logger.debug("Fetched all Twitter elements")

    # Map elements found to correct positions
    Parser.map_elements_to_positions(elements, data_list, return_list)
  end

  @spec parse_list_with_index(list(), []) :: list()
  def parse_list_with_index(return_list, []) do
    Logger.debug("Parsing element list for Twitter elements, but there's none to get")
    return_list
  end

  @doc """
  Processes a list of Twitter codes and fetch tweets

  * `_codes` is a list of twitter snowflakes ids
  """
  @spec parse_twitter_codes_list(List.t, Account.t) :: List.t
  defp parse_twitter_codes_list([codes | tail = _codes], account) do
    Logger.debug("Fetching Tweet elements")
    try do
      tweets = Enum.join(codes, ",")
      Logger.debug(inspect tweets)
      if %{status: :ok} == ConnectTwitter.twitter_has_token(account) do
          ConnectTwitter.configure_ex_twitter(account)
      end
      tweets = Enum.map(ExTwitter.lookup_status(tweets, [tweet_mode: "extended"]), fn tweet -> process_tweet(tweet) end)
      Logger.debug(inspect tweets)
      tweets ++ parse_twitter_codes_list(tail, account)
    rescue
      e in ExTwitter.Error ->
        Logger.error("Twitter Error: #{e.message}")
      e in Poison.SyntaxError ->
        Logger.error(inspect e)
    end
  end

  @doc false
  @spec parse_twitter_codes_list(List.t, Account.t) :: List.t
  defp parse_twitter_codes_list([], %Account{} = _account) do
    []
  end

  @doc """
  Parses a single Twitter URL to a Tweet element
  """
  @spec parse_single_url(String.t, Account.t) :: Pickweaver.Servces.Parser.Tweet.t
  def parse_single_url(tweet_url, %Account{} = account) do
    [code] = Regex.run(~r/http(?:s)?:\/\/(?:www.)?twitter\.com\/[a-zA-Z0-9_|i\/web\/]+\/status\/([0-9]+)/iu, tweet_url, [capture: :all_but_first])
    if %{status: :ok} == ConnectTwitter.twitter_has_token(account) do
      ConnectTwitter.configure_ex_twitter(account)
    end
    tweet = ExTwitter.lookup_status(code, [tweet_mode: "extended"])
    process_tweet(tweet)
  end

  @doc """
  Find the best video between all offered by comparing their bitrate
  """
  @spec find_best_video(list(map()), map()) :: map()
  defp find_best_video(video_list, video \\ %{bitrate: 0})
  defp find_best_video([source|tail], video) do
    if Map.has_key?(source, :bitrate) && source.bitrate > video.bitrate do
      find_best_video(tail, source)
    else
      find_best_video(tail, video)
    end
  end

  @spec find_best_video([], map()) :: map()
  defp find_best_video([], video) do
    video
  end

  @doc """
  Creates a `Pickweaver.Services.Parser.Tweet` struct from `ExTwitter.Model.Tweet` information
  """
  @spec process_tweet(ExTwitter.Model.Tweet.t) :: Pickweaver.Services.Parser.Tweet.t
  def process_tweet(%ExTwitter.Model.Tweet{} = tweet) do
    Logger.debug("Raw tweet")
    Logger.debug(inspect tweet)
    imgs = if tweet.extended_entities[:media] do
      Enum.filter(Enum.map(tweet.extended_entities.media, fn img ->
        if img.type == "photo" do
          Pickweaver.MediaProxy.gen_url(img.media_url_https)
        end
      end), fn img -> img != nil end)
    else
      []
    end
    videos = if tweet.extended_entities[:media] do
      Enum.filter(Enum.map(tweet.extended_entities.media, fn video ->
        if video.type == "video" do
          %{"preview" => Pickweaver.MediaProxy.gen_url(video.media_url_https), "video" => find_best_video(video.video_info.variants).url}
        end
      end), fn video -> video != nil end)
    else
      []
    end
    mentions = if tweet.entities[:user_mentions] do tweet.entities.user_mentions else [] end
    hashtags = if tweet.entities[:hashtags] do tweet.entities.hashtags else [] end
    %Tweet{
      id: tweet.id_str,
      user: tweet.user,
      text: cleanText(tweet),
      created_at: tweet.created_at,
      images: imgs,
      videos: videos,
      force_url: "https://twitter.com/#{tweet.user.screen_name}/status/#{tweet.id_str}",
      hashtags: hashtags,
      mentions: mentions,
    }
  end

  @doc """
  Unused Get Twitter Rate API Status

  TODO : Use me or remove me
  """
  defp get_twitter_rate_limit() do
    ExTwitter.rate_limit_status()
  end

  @doc """
  Cleans text from a tweet by replacing t.co links with originals and stripping media URLs
  """
  @spec cleanText(ExTwitter.Model.Tweet.t) :: String.t
  defp cleanText(%ExTwitter.Model.Tweet{} = tweet) do
    text = if tweet.full_text == nil do tweet.text else tweet.full_text end
    text = processLinks(text, tweet)
    if tweet.entities[:media] do
      stripImageURLs(text, tweet.entities.media)
    else
      text
    end
  end

  @doc """
  Replaces multiple t.co links in the tweet text with their original link
  """
  @spec processLinks(String.t, ExTwitter.Model.Tweet.t) :: String.t
  defp processLinks(text, tweet) do
    proccessLink(text, tweet.entities.urls, tweet.id_str)
  end

  @doc """
  Replaces a t.co link in the tweet text with their original link
  """
  @spec proccessLink(String.t, list(map()), String.t) :: String.t
  defp proccessLink(text, [head | tail], id) do
    # If the URL provided is the tweet URL itself, don't try to replace it
    text = if head.expanded_url != "https://twitter.com/i/web/status/" <> id do
        String.replace(text, head.url, head.expanded_url)
      else
        text
      end
    proccessLink(text, tail, id)
  end

  @spec proccessLink(String.t, [], String.t) :: String.t
  defp proccessLink(text, [], _id) do
    text
  end

  @doc """
  Strips Image URLs from the tweet text
  """
  @spec stripImageURLs(String.t, list(map())) :: String.t
  defp stripImageURLs(text, [head | tail]) do
    Logger.debug("Removing #{head.url} from text tweet")
    text = if String.contains?(text, head.url) do
      String.replace(text, head.url, "")
    else
      text
    end
    stripImageURLs(text, tail)
  end

  @spec stripImageURLs(String.t, list()) :: String.t
  defp stripImageURLs(text, []) do
    text
  end
end
