defmodule Pickweaver.Services.Parser.Youtube do
  @moduledoc """
  YouTube Parser that connects to the YouTube API to get metadata on a video
  """
  require Logger
  alias Pickweaver.Services.Parser.Youtube

  defstruct [:id, :title, :description, :thumbnails, :channelTitle, :viewCount, :duration, :published_at, :force_url]
  @type t :: %Youtube{id: String.t, title: String.t, description: String.t, published_at: %DateTime{}, thumbnails: list(), force_url: String.t, channelTitle: String.t, viewCount: integer(), duration: float()}

  @doc """
  Parses a YouTube URL and returns a `Pickweaver.Services.Parser.YouTube` element.

  ## Examples

      iex> parse_youtube_url("https://www.youtube.com/watch?v=t5tBsVX5g0g")
      {:ok, %Youtube{}}
  """
  @spec parse_youtube_url(String.t) :: {:ok, Youtube.t}
  def parse_youtube_url(url) do
    Logger.debug("Using YouTube API parser")
    {url, code} = get_code_from_url(url)
    {:ok, %{process_youtube([code]) | force_url: url}}
  end

  @doc """
  Maps several YouTube URLs to return a list of `Pickweaver.Services.Parser.YouTube` elements.

  ## Examples

      iex> parse_youtube_urls(["https://www.youtube.com/watch?v=t5tBsVX5g0g", "https://www.youtube.com/watch?v=323-v4kDdnk"])
      [%Youtube{}, %Youtube{}]
  """
  @spec parse_youtube_urls(list(String.t)) :: list(Youtube.t)
  def parse_youtube_urls(urls) do
    urls
    |> Enum.map(fn url -> get_code_from_url(url) end)
    |> process_youtube_list()
  end

  @spec process_youtube(String.t) :: Youtube.t
  defp process_youtube(video_id) do
    [body] = get_json_data([video_id])
    map_youtube(body)
  end

  @spec process_youtube_list([]) :: []
  defp process_youtube_list([]) do
    []
  end

  @spec process_youtube_list(list({String.t, String.t})) :: list(Youtube.t)
  defp process_youtube_list(codes_url) do
    codes_url
    |> Enum.map(fn {_url, code} -> code end)
    |> get_json_data()
    |> Enum.map(fn body -> map_youtube(body) end)
  end

  @spec map_youtube(map()) :: Youtube.t
  defp map_youtube(body) do
    %Youtube{
      id: body.id,
      title: body.snippet.title,
      description: short_description(body.snippet.description),
      thumbnails: body.snippet.thumbnails,
      channelTitle: body.snippet.channelTitle,
      viewCount: body.statistics.viewCount,
      duration: body.contentDetails.duration,
      published_at: body.snippet.publishedAt,
    }
  end

  @spec get_json_data(list(String.t)) :: map()
  defp get_json_data(video_ids) do
    video_ids = Enum.join(video_ids, ",")
    {:ok, res} = HTTPoison.get "https://www.googleapis.com/youtube/v3/videos?id=#{video_ids}&key=#{Application.fetch_env!(:pickweaver, :youtube_key)}&part=snippet,statistics,contentDetails&fields=items(id,snippet(title,description,channelTitle,thumbnails(high),publishedAt),statistics(viewCount),contentDetails(duration))"
    Poison.decode!(res.body, keys: :atoms).items
  end

  @spec get_code_from_url(String.t) :: {String.t, String.t}
  defp get_code_from_url(url) do
    [_, code] = Regex.run(~r/(?:youtube\.com\/\S*(?:(?:\/e(?:mbed))?\/|watch\?(?:\S*?&?v\=))|youtu\.be\/)([a-zA-Z0-9_-]{6,11})/iu, url)
    url = "https://www.youtube.com/watch?v=" <> code
    {url, code}
  end

  @spec short_description(String.t) :: String.t
  defp short_description(desc) do
    pos = case :binary.match String.slice(desc, 180..-1), " " do
      {pos, _} ->
        pos
      :nomatch ->
        0
    end
    Logger.debug("Nombre de caractères en plus")
    Logger.debug(pos)
    String.slice(desc, 0, 180 + pos) <> "…"
  end
end
