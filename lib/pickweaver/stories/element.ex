defmodule Pickweaver.Stories.Element do
  @moduledoc """
  Represents an element from a story
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Pickweaver.Stories.Element

  @type t :: %Element{data: map(), id: Ecto.UUID.t, inserted_at: DateTime.t, updated_at: DateTime.t, type: TypeEnum.t, url: String.t}

  @primary_key {:id, Ecto.UUID, autogenerate: true}
  @foreign_key_type :uuid
  schema "elements" do
    field :url, :string
    field :data, :map
    field :type, TypeEnum

    timestamps()
  end

  @doc false
  def changeset(%Element{} = element, attrs) do
    element
    |> cast(attrs, [:url, :data, :type])
    |> validate_required([:data, :type])
    |> validate_length(:url, max: 2048)
  end
end
