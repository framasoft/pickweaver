defmodule Pickweaver.Stories do
  @moduledoc """
  The Stories context.
  """

  require Logger
  import Ecto.Query, warn: false
  alias Pickweaver.Repo

  alias Pickweaver.Stories.{Story, Element}
  alias Pickweaver.Accounts.Account

  @doc """
  Returns the list of stories.

  ## Examples

      iex> list_stories()
      [%Story{}, ...]

  """
  def list_stories(params \\ %{}) do
    query = from s in Story,
      preload: [:account, :media],
      order_by: [desc: s.inserted_at]
    Repo.paginate(query, params)
  end

  @default_own_index_params %{"page" => 1, "order_by" => "inserted_at", "direction" => "desc"}
  @doc """
  Returns the list of stories for an account
  """
  def list_stories_for_account(%Account{} = account, params \\ @default_own_index_params) do
    final_params = Map.merge(@default_own_index_params, params)
    query = from s in Story,
       where: s.account_id == ^account.id,
       preload: [:account, :media],
       order_by: [{^String.to_atom(final_params["direction"]), field(s, ^String.to_atom(final_params["order_by"]))}]
    Repo.paginate(query, final_params)
  end

  @doc """
  Returns the list of stories for an account slug
  """
  def list_published_stories_for_account_slug(account_slug, params \\ %{}) do
    query = from s in Story,
      join: a in Account, where: a.id == s.account_id,
      where: a.slug == ^account_slug and s.published,
      preload: [:account, :media],
      order_by: [desc: s.inserted_at]
    Repo.paginate(query, params)
  end

  @default_search_params %{"search" => "", "page" => 1, "order_by" => "inserted_at", "direction" => "desc"}
  @doc """
  Search through user's own stories
  """
  def search_stories_for_account(%Account{} = account, params \\ @default_search_params) do
    query = from s in Story,
                 where: s.account_id == ^account.id and ilike(s.title, ^like_sanitize(params["search"])),
                 preload: [:account, :media],
                 order_by: [{^String.to_atom(params["direction"]), field(s, ^String.to_atom(params["order_by"]))}]
    Repo.paginate(query, params)
  end

  @doc """
  Search through public stories
  """
  def search_stories(params \\ @default_search_params) do
    final_params = Map.merge(@default_search_params, params)
    query = from s in Story,
             where: ilike(s.title, ^like_sanitize(final_params["search"])) and s.published,
             preload: [:account, :media],
             order_by: [{^String.to_atom(final_params["direction"]), field(s, ^String.to_atom(final_params["order_by"]))}]
    Repo.paginate(query, final_params)
  end

  defp like_sanitize(value) do
    "%" <> String.replace(value, ~r/([\\%_])/, "\\1") <> "%"
  end

  @doc """
  Gets a single story.

  Raises `Ecto.NoResultsError` if the Story does not exist.

  ## Examples

      iex> get_story!(123)
      %Story{}

      iex> get_story!(456)
      ** (Ecto.NoResultsError)

  """
  def get_story!(id) do
    story = Repo.get!(Story, id)
    Repo.preload(story, [:account, :media])
  end

  @doc """
  Gets a single story by its slug

  Returns nil if there's no story with this slug

  ## Examples

      iex> get_story_by_slug("my story")
      %Story{}

      iex> get_story_by_slug("inexistant")
      nil
  """
  @spec get_story_by_slug(String.t, integer, integer) :: %Story{}
  def get_story_by_slug(slug, page \\ 1, page_size \\ 20, load_all \\ false) do
    Logger.debug("Calling get_story_by_slug with page #{page} and page_size #{page_size}")
    story = Repo.get_by(Story, slug: slug)
    story = Repo.preload story, [:account, :media]
    if !is_nil(story) do
      bring_elements_with_story(story, page, page_size, load_all)
    end
  end

  @doc """
  Gets a single story by its slug and the username

  Returns nil if there's no story with this slug

  ## Examples

      iex> get_story_by_slug_and_username("my story", "thomas")
      %Story{}

      iex> get_story_by_slug_and_username("inexistant", "nooo")
      nil
  """
  @spec get_story_by_slug_and_username(String.t, String.t, integer, integer, boolean) :: %Story{}
  def get_story_by_slug_and_username(slug, username, page \\ 1, page_size \\ 20, load_all \\ false) do
    Logger.debug("Calling get_story_by_slug_and username with page #{page} and page_size #{page_size}")
    story = Repo.one(
      from s in Story,
        join: a in Account,
        on: a.id == s.account_id and a.slug == ^username,
        where: s.slug == ^slug
    )
    story = Repo.preload story, [:account, :media]
    if !is_nil story do
      bring_elements_with_story(story, page, page_size, load_all)
    end
  end

  defp bring_elements_with_story(story, page \\ 1, page_size \\ 20, load_all \\ false) do
    nb_elements = length(story.elements)
    sliced_elements = if !load_all do
      Enum.slice(story.elements, ((page - 1) * page_size)..((page * page_size) - 1))
    else
      story.elements
    end
    elements = Enum.map(sliced_elements, fn element -> Repo.one(from e in Element, where: e.id == ^element) end)
    {
      :ok,
      Map.put(story, :elements, %{
        entries: elements,
        total_entries: nb_elements,
        total_pages: find_pages_for_elements(nb_elements, page_size),
        page_number: page,
        page_size: page_size
      })
    }
  end

  defp find_pages_for_elements(nb_elements, page_size) do
    res = div(nb_elements, page_size)
    case rem(nb_elements, page_size) do
      0 ->
        res
      _ ->
        res + 1
    end
  end

  @doc """
  Creates a story.

  ## Examples

      iex> create_story(%{field: value})
      {:ok, %Story{}}

      iex> create_story(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_story(attrs \\ %{}) do
    attrs = add_header(attrs)
    story_inserted = %Story{}
      |> Story.changeset(attrs)
      |> Repo.insert()
    case story_inserted do
      {:ok, story} ->
        story = Repo.preload story, [:account, :media]
        bring_elements_with_story(story)
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @doc """
  Updates a story.

  ## Examples

      iex> update_story(story, %{field: new_value})
      {:ok, %Story{}}

      iex> update_story(story, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_story(%Story{} = story, attrs) do
    attrs = add_header(attrs)
    story
    |> Story.changeset(attrs)
    |> Repo.update()
  end


  defp add_header(attrs) do
    # TODO : This is ugly : we should use string keys everywhere !
    {key, elements} = if Map.has_key?(attrs, :elements) do
      {:header, attrs.elements}
    else
      {"header", attrs["elements"]}
    end
    _attrs = Map.put(attrs, key, get_header(elements))
  end

  @doc """
  Deletes a Story.

  ## Examples

      iex> delete_story(story)
      {:ok, %Story{}}

      iex> delete_story(story)
      {:error, %Ecto.Changeset{}}

  """
  def delete_story(%Story{} = story) do
    Repo.delete(story)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking story changes.

  ## Examples

      iex> change_story(story)
      %Ecto.Changeset{source: %Story{}}

  """
  def change_story(%Story{} = story) do
    Story.changeset(story, %{})
  end

  @doc """
  Count how many stories there are for a given account
  """
  @spec count_published_stories_for_account(%Account{}) :: integer()
  def count_published_stories_for_account(%Account{} = account) do
    Repo.one(from s in Story, select: count(s.id), where: s.account_id == ^account.id and s.published == true)
  end

  @doc """
  Returns an `Pickweaver.Stories.Element` by it's uuid

  ## Examples

      iex> get_element(123)
      %Element{}
  """
  @spec get_element!(integer()) :: %Element{}
  def get_element!(id) do
    Repo.get!(Element, id)
  end

  @doc """
  Returns an eventual `Pickweaver.Stories.Element` by it's URL

  ## Examples

      iex> get_element_by_url("https://twitter.com/nitot/status/957963447513296897")
      %Element{}
  """
  @spec get_element_by_url(String.t) :: %Element{}
  def get_element_by_url(url) do
    element = Repo.get_by(Element, url: url)
    if is_nil element do
      nil
    else
      Map.put(element, :force_url, url)
    end
  end

  @doc """
  Creates multiple elements

  ## Examples

      iex> create_elements(elements)
      [%Element{}]

  """
  @spec create_elements(list(struct())) :: list(%Element{})
  def create_elements(elements) do
    Enum.map(elements, fn element ->
      case url_from_element(element) do
        nil ->
          Logger.debug("Element doesn't have a force_url : so it's text")
          {:ok, produced_element} = create_element(element)
          produced_element
        url ->
          return_element(element, url)
      end
    end)
  end

  defp return_element(element, url) do
    Logger.debug("Element does have a force_url")
    case get_element_by_url(url) do
      nil ->
        Logger.debug("Created new element")
        {:ok, produced_element} = create_element(element)
        produced_element
      produced_element ->
        Logger.debug("Found existing element")
        produced_element
    end
  end

  def create_or_return_element(element) do
    case url_from_element(element) do
      nil ->
        Logger.debug("Element doesn't have a force_url : so it's text")
        {:ok, produced_element} = create_element(element)
        produced_element
      url ->
        return_element(element, url)
    end
  end

  @doc """
  Creates an element

  ## Examples

      iex> create_element(element)
      %Element{}

  """
  @spec create_element(struct()) :: %Element{}
  def create_element(element) do
    %Element{}
     |> Element.changeset(%{"data" => maps_with_string_keys(Map.from_struct(element)), "url" => url_from_element(element), "type" => type_from_element(element)})
     |> Repo.insert()
  end

  defp maps_with_string_keys(element) do
    Map.new(element, fn {k, v} -> {Atom.to_string(k), v} end)
  end

  defp type_from_element(element) do
    element.__struct__
    |> Module.split()
    |> Enum.take(-1)
    |> List.first()
    |> String.downcase()
    |> String.to_existing_atom()
  end

  defp element_is_text(element) do
    type_from_element(element) != :text
  end

  defp url_from_element(element) do
    if element_is_text(element) do
      element.force_url
    else
      nil
    end
  end

  defp get_header([head|tail]) do
    Logger.debug("Getting header for story")
    header = get_header_from_element(get_element!(head))
    case header do
      nil ->
        get_header(tail)
      _ ->
        header
    end
  end

  defp get_header([]) do
    nil
  end

  # TODO : Just for tests. Change me ?
  defp get_header(nil) do
    nil
  end

  defp get_header_from_element(%Element{} = element) do
    Logger.debug("trying to get header from element")
    Logger.debug(inspect element)
    case element.type do
      :tweet ->
        with [img] <- element.data["images"] do
          img
        else
          _ -> nil
        end
      :opengraph ->
        element.data["image"]
      _ ->
        nil
    end
  end
end
