defmodule Pickweaver.Import do
  @moduledoc """
  Represents an import to process
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Pickweaver.Import
  alias Pickweaver.Accounts.Account


  schema "imports" do
    field :tentatives, :integer, default: 0
    field :url, :string
    field :status, :integer, default: 0
    belongs_to :account, Account

    timestamps()
  end

  @doc false
  def changeset(%Import{} = import, attrs) do
    import
    |> cast(attrs, [:url, :tentatives, :account_id, :status])
    |> unique_constraint(:url, name: :index_unique_imports_url_account_id)
    |> validate_required([:url, :account_id])
  end
end
