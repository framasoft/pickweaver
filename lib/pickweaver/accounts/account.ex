defmodule Pickweaver.Accounts.Account do
  @moduledoc """
  Represents an user account
  """
  use Ecto.Schema
  require Logger
  import Ecto.Changeset
  import Exgravatar
  alias Pickweaver.Accounts.{Account, UsernameSlug}

  schema "accounts" do
    field :username, :string
    field :email, :string
    field :password_hash, :string
    field :password, :string, virtual: true
    field :role, :integer, default: 0
    field :display_name, :string
    field :avatar, :string
    field :confirmed_at, :utc_datetime
    field :confirmation_sent_at, :utc_datetime
    field :confirmation_token, :string
    field :reset_password_sent_at, :utc_datetime
    field :reset_password_token, :string

    field :slug, UsernameSlug.Type

    timestamps()
  end

  @doc false
  def changeset(%Account{} = account, attrs) do
    account
    |> cast(attrs, [:username, :email, :password_hash, :password, :role, :display_name, :avatar, :confirmed_at, :confirmation_sent_at, :confirmation_token, :reset_password_sent_at, :reset_password_token])
    |> validate_required([:username, :email])
    |> unique_constraint(:username, [message: "registration.error.username_already_used"])
    |> unique_constraint(:email, [message: "registration.error.email_already_used"])
    |> validate_format(:email, ~r/@/)
    |> validate_length(:password, min: 6, max: 100, message: "registration.error.password_too_short")
  end

  def registration_changeset(struct, params) do
    struct
    |> changeset(params)
    |> cast(params, ~w(password)a, [])
    |> validate_required([:username, :email, :password])
    |> validate_length(:password, min: 6, max: 100, message: "registration.error.password_too_short")
    |> UsernameSlug.maybe_generate_slug()
    |> UsernameSlug.unique_constraint()
    |> gravatar()
    |> hash_password()
    |> save_confirmation_token()
    |> unique_constraint(:confirmation_token, [message: "regisration.error.confirmation_token_already_in_use"])
  end

  def send_password_reset_changeset(%Account{} = account, attrs) do
    account
    |> cast(attrs, [:reset_password_token, :reset_password_sent_at])
  end

  def password_reset_changeset(%Account{} = account, attrs) do
    account
    |> cast(attrs, [:password, :reset_password_token, :reset_password_sent_at])
    |> validate_length(:password, min: 6, max: 100, message: "registration.error.password_too_short")
    |> hash_password()
  end

  defp save_confirmation_token(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true,
        changes: %{email: _email}} ->
          changeset = put_change(changeset, :confirmation_token, random_string(30))
          put_change(changeset, :confirmation_sent_at, DateTime.utc_now())
      _ ->
        changeset
    end
  end

  defp random_string(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64
  end

  defp hash_password(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true,
        changes: %{password: password}} ->
        put_change(changeset,
          :password_hash,
          Comeonin.Argon2.hashpwsalt(password))
      _ ->
        changeset
    end
  end

  defp gravatar(changeset) do
    Logger.debug(inspect changeset)
    case changeset do
      %Ecto.Changeset{valid?: true,
        changes: %{email: email}} ->
          url = gravatar_url(email, d: "404")
          case HTTPoison.get(url, [], [ssl: [{:versions, [:'tlsv1.2']}]]) do
            {:ok, %HTTPoison.Response{status_code: 200}} ->
              put_change(changeset, :avatar, url)
            _ ->
              changeset
          end
      _ ->
        changeset
    end
  end
end
