# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config
import Logger

# General application configuration
config :pickweaver,
  ecto_repos: [Pickweaver.Repo]

# Configures the endpoint
config :pickweaver, PickweaverWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "WR4fe7qMymkHx5RI9USBOgNbRG8Gv5XXBwMse0oaMIYZiGAwG9CstejCQKaX8zSt",
  render_errors: [view: PickweaverWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Pickweaver.PubSub,
           adapter: Phoenix.PubSub.PG2]

config :mime, :types, %{
  "application/json" => ["json"]
}

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :pickweaver, Pickweaver.Mailer,
       adapter: Bamboo.SMTPAdapter,
       server: "localhost",
       hostname: "localhost",
       port: 25,
       username: nil, # or {:system, "SMTP_USERNAME"}
       password: nil, # or {:system, "SMTP_PASSWORD"}
       tls: :if_available, # can be `:always` or `:never`
       allowed_tls_versions: [:tlsv1, :"tlsv1.1", :"tlsv1.2"], # or {":system", ALLOWED_TLS_VERSIONS"} w/ comma seprated values (e.g. "tlsv1.1,tlsv1.2")
       ssl: false, # can be `true`
       retries: 1,
       no_mx_lookups: false # can be `true`

config :pickweaver, :media_proxy,
  redirect_on_failure: true

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
config :extwitter, :oauth, [
   consumer_key: System.get_env("Twitter_Consumer_Key"),
   consumer_secret: System.get_env("Twitter_Consumer_Secret"),
   access_token: System.get_env("Twitter_Access_Token"),
   access_token_secret: System.get_env("Twitter_Access_Token_Secret")
]

config :pickweaver,
   youtube_key: System.get_env("YouTube_Key")

config :pickweaver, PickweaverWeb.Guardian,
       issuer: "pickweaver",
       secret_key: "0Gmquox+GlHyMomWSka3AIZ8kWN2zpdWYM4GNEba3vBuYWtRbp/7sR0tMzBL4D8n"

config :guardian, Guardian.DB,
       repo: Pickweaver.Repo,
       schema_name: "guardian_tokens", # default
       sweep_interval: 60 # default: 60 minutes

config :extus,
       storage: ExTus.Storage.Local,
       base_dir: "upload",
       upload_url: "/files",
       expired_after: 24 * 60 * 60 * 1000, #clean uncompleted upload after 1 day
       clean_interval: 30 * 60 * 1000 # start cleaning job after 30min

config :pickweaver, :phoenix_swagger,
       swagger_files: %{
         "priv/static/swagger.json" => [
           router: PickweaverWeb.Router,     # phoenix routes will be converted to swagger paths
           endpoint: PickweaverWeb.Endpoint  # (optional) endpoint config used to set host, port and https schemes.
         ]
       }
