@moduledoc """
A schema is a keyword list which represents how to map, transform, and validate
configuration values parsed from the .conf file. The following is an explanation of
each key in the schema definition in order of appearance, and how to use them.

## Import

A list of application names (as atoms), which represent apps to load modules from
which you can then reference in your schema definition. This is how you import your
own custom Validator/Transform modules, or general utility modules for use in
validator/transform functions in the schema. For example, if you have an application
`:foo` which contains a custom Transform module, you would add it to your schema like so:

`[ import: [:foo], ..., transforms: ["myapp.some.setting": MyApp.SomeTransform]]`

## Extends

A list of application names (as atoms), which contain schemas that you want to extend
with this schema. By extending a schema, you effectively re-use definitions in the
extended schema. You may also override definitions from the extended schema by redefining them
in the extending schema. You use `:extends` like so:

`[ extends: [:foo], ... ]`

## Mappings

Mappings define how to interpret settings in the .conf when they are translated to
runtime configuration. They also define how the .conf will be generated, things like
documention, @see references, example values, etc.

See the moduledoc for `Conform.Schema.Mapping` for more details.

## Transforms

Transforms are custom functions which are executed to build the value which will be
stored at the path defined by the key. Transforms have access to the current config
state via the `Conform.Conf` module, and can use that to build complex configuration
from a combination of other config values.

See the moduledoc for `Conform.Schema.Transform` for more details and examples.

## Validators

Validators are simple functions which take two arguments, the value to be validated,
and arguments provided to the validator (used only by custom validators). A validator
checks the value, and returns `:ok` if it is valid, `{:warn, message}` if it is valid,
but should be brought to the users attention, or `{:error, message}` if it is invalid.

See the moduledoc for `Conform.Schema.Validator` for more details and examples.
"""
[
  extends: [],
  import: [],
  mappings: [
    "pickweaver.Elixir.PickweaverWeb.Endpoint.instance": [
      commented: false,
      datatype: :binary,
      default: "framastory.org",
      doc: "Instance hostname",
      hidden: false,
      to: "pickweaver.Elixir.PickweaverWeb.Endpoint.instance"
    ],
    "pickweaver.Elixir.PickweaverWeb.Endpoint.http.port": [
      commented: false,
      datatype: :integer,
      default: 4000,
      doc: "HTTP Server configuration port",
      hidden: false,
      to: "pickweaver.Elixir.PickweaverWeb.Endpoint.http.port"
    ],
    "pickweaver.Elixir.PickweaverWeb.Endpoint.url.host": [
      commented: false,
      datatype: :binary,
      default: "localhost",
      doc: "The host to generate urls from",
      hidden: false,
      to: "pickweaver.Elixir.PickweaverWeb.Endpoint.url.host"
    ],
    "pickweaver.Elixir.PickweaverWeb.Endpoint.url.port": [
      commented: false,
      datatype: :integer,
      default: 443,
      doc: "The port to generate urls from",
      hidden: false,
      to: "pickweaver.Elixir.PickweaverWeb.Endpoint.url.port"
    ],
    "pickweaver.media_proxy.redirect_on_failure": [
      commented: false,
      datatype: :atom,
      default: true,
      doc: "Redirect to original media if proxy fails or return 502",
      hidden: false,
      to: "pickweaver.media_proxy.redirect_on_failure"
    ],
    "pickweaver.Elixir.Pickweaver.Repo.username": [
      commented: false,
      datatype: :binary,
      default: "elixir",
      doc: "Database user",
      hidden: false,
      to: "pickweaver.Elixir.Pickweaver.Repo.username"
    ],
    "pickweaver.Elixir.Pickweaver.Repo.password": [
      commented: false,
      datatype: :binary,
      default: "elixir",
      doc: "Database password",
      hidden: false,
      to: "pickweaver.Elixir.Pickweaver.Repo.password"
    ],
    "pickweaver.Elixir.Pickweaver.Repo.database": [
      commented: false,
      datatype: :binary,
      default: "pickweaver_dev",
      doc: "Database name",
      hidden: false,
      to: "pickweaver.Elixir.Pickweaver.Repo.database"
    ],
    "pickweaver.Elixir.Pickweaver.Repo.hostname": [
      commented: false,
      datatype: :binary,
      default: "localhost",
      doc: "Database host",
      hidden: false,
      to: "pickweaver.Elixir.Pickweaver.Repo.hostname"
    ],
    "pickweaver.Elixir.Pickweaver.Repo.pool_size": [
      commented: false,
      datatype: :integer,
      default: 10,
      doc: "PostgreSQL Pool Size",
      hidden: false,
      to: "pickweaver.Elixir.Pickweaver.Repo.pool_size"
    ],
    "pickweaver.Elixir.PickweaverWeb.Endpoint.email_from": [
      commented: false,
      datatype: :binary,
      default: "test@local.tld",
      doc: "Email from address",
      hidden: false,
      to: "pickweaver.Elixir.PickweaverWeb.Endpoint.email_from"
    ],
    "pickweaver.Elixir.PickweaverWeb.Endpoint.reply_to": [
      commented: false,
      datatype: :binary,
      default: "reply@local.tld",
      doc: "Email reply_to address",
      hidden: false,
      to: "pickweaver.Elixir.PickweaverWeb.Endpoint.reply_to"
    ],
    "pickweaver.Elixir.Pickweaver.Mailer.server": [
      commented: false,
      datatype: :binary,
      default: "localhost",
      doc: "Mailer server",
      hidden: false,
      to: "pickweaver.Elixir.Pickweaver.Mailer.server"
    ],
    "pickweaver.Elixir.Pickweaver.Mailer.hostname": [
      commented: false,
      datatype: :binary,
      default: "localhost",
      doc: "Mailer hostname",
      hidden: false,
      to: "pickweaver.Elixir.Pickweaver.Mailer.hostname"
    ],
    "pickweaver.Elixir.Pickweaver.Mailer.port": [
      commented: false,
      datatype: :integer,
      default: 25,
      doc: "Mailer port",
      hidden: false,
      to: "pickweaver.Elixir.Pickweaver.Mailer.port"
    ],
    "pickweaver.Elixir.Pickweaver.Mailer.username": [
      commented: true,
      datatype: :atom,
      doc: "Mailer username",
      hidden: false,
      to: "pickweaver.Elixir.Pickweaver.Mailer.username"
    ],
    "pickweaver.Elixir.Pickweaver.Mailer.password": [
      commented: true,
      datatype: :atom,
      doc: "Mailer password",
      hidden: false,
      to: "pickweaver.Elixir.Pickweaver.Mailer.password"
    ],
    "pickweaver.Elixir.Pickweaver.Mailer.tls": [
      commented: false,
      datatype: :atom,
      default: :if_available,
      doc: "Whether to use tls",
      hidden: false,
      to: "pickweaver.Elixir.Pickweaver.Mailer.tls"
    ],
    "pickweaver.Elixir.Pickweaver.Mailer.allowed_tls_versions": [
      commented: false,
      datatype: [
        list: :atom
      ],
      default: [
        :tlsv1,
        :"tlsv1.1",
        :"tlsv1.2"
      ],
      doc: "Allowed tls versions",
      hidden: false,
      to: "pickweaver.Elixir.Pickweaver.Mailer.allowed_tls_versions"
    ],
    "pickweaver.Elixir.Pickweaver.Mailer.ssl": [
      commented: false,
      datatype: :atom,
      default: false,
      doc: "Whether to use ssl",
      hidden: false,
      to: "pickweaver.Elixir.Pickweaver.Mailer.ssl"
    ],
    "pickweaver.Elixir.Pickweaver.Mailer.retries": [
      commented: false,
      datatype: :integer,
      default: 1,
      doc: "Number of allowed retries",
      hidden: false,
      to: "pickweaver.Elixir.Pickweaver.Mailer.retries"
    ],
    "pickweaver.Elixir.Pickweaver.Mailer.no_mx_lookups": [
      commented: false,
      datatype: :atom,
      default: false,
      doc: "Whether or not to do MX Lookups on email domains before processing any emails",
      hidden: false,
      to: "pickweaver.Elixir.Pickweaver.Mailer.no_mx_lookups"
    ],
    "pickweaver.Elixir.Pickweaver.Mailer.adapter": [
      commented: false,
      datatype: :atom,
      default: Bamboo.LocalAdapter,
      doc: "Mailer adapter",
      hidden: true,
      to: "pickweaver.Elixir.Pickweaver.Mailer.adapter"
    ],
    "extwitter.oauth.consumer_key": [
      commented: false,
      datatype: :binary,
      default: "twitter consumer key",
      doc: "Twitter consumer key",
      hidden: false,
      to: "extwitter.oauth.consumer_key"
    ],
    "extwitter.oauth.consumer_secret": [
      commented: false,
      datatype: :binary,
      default: "twitter consumer secret",
      doc: "Twitter consumer secret",
      hidden: false,
      to: "extwitter.oauth.consumer_secret"
    ],
    "extwitter.oauth.access_token": [
      commented: false,
      datatype: :binary,
      default: "twitter access token",
      doc: "Twitter Access Token",
      hidden: false,
      to: "extwitter.oauth.access_token"
    ],
    "extwitter.oauth.access_token_secret": [
      commented: false,
      datatype: :binary,
      default: "twitter access token secret",
      doc: "Twitter Access Token Secret",
      hidden: false,
      to: "extwitter.oauth.access_token_secret"
    ],
    "pickweaver.youtube_key": [
      commented: false,
      datatype: :binary,
      default: "youtube key",
      doc: "YouTube key",
      hidden: false,
      to: "pickweaver.youtube_key"
    ],
    "mime.types": [
      commented: false,
      datatype: :binary,
      doc: "Provide documentation for mime.types here.",
      hidden: true,
      to: "mime.types"
    ],
    "logger.console.metadata": [
      commented: false,
      datatype: [
        list: :atom
      ],
      default: [
        :request_id
      ],
      doc: "Provide documentation for logger.console.metadata here.",
      hidden: false,
      to: "logger.console.metadata"
    ],
    "logger.console.format": [
      commented: false,
      datatype: :binary,
      default: """
      [$level] $message
      """,
      doc: "Logger console format",
      hidden: false,
      to: "logger.console.format"
    ],
    "logger.console.truncate": [
      commented: false,
      datatype: :integer,
      default: 20000,
      doc: "Trucate console after x characters.",
      hidden: false,
      to: "logger.console.truncate"
    ],
    "phoenix.stacktrace_depth": [
      commented: false,
      datatype: :integer,
      default: 20,
      doc: "Size of stacktrace",
      hidden: false,
      to: "phoenix.stacktrace_depth"
    ],
    "guardian.Elixir.Guardian.DB.repo": [
      commented: false,
      datatype: :atom,
      default: Pickweaver.Repo,
      doc: "Repo for Guardian tokens",
      hidden: true,
      to: "guardian.Elixir.Guardian.DB.repo"
    ],
    "guardian.Elixir.Guardian.DB.schema_name": [
      commented: false,
      datatype: :binary,
      default: "guardian_tokens",
      doc: "Table for Guardian tokens",
      hidden: true,
      to: "guardian.Elixir.Guardian.DB.schema_name"
    ],
    "guardian.Elixir.Guardian.DB.sweep_interval": [
      commented: false,
      datatype: :integer,
      default: 60,
      doc: "Time in minutes after which tokens are swept",
      hidden: false,
      to: "guardian.Elixir.Guardian.DB.sweep_interval"
    ],
    "extus.storage": [
      commented: false,
      datatype: :atom,
      default: ExTus.Storage.Local,
      doc: "Extus Storage adapter",
      hidden: true,
      to: "extus.storage"
    ],
    "extus.base_dir": [
      commented: false,
      datatype: :binary,
      default: "upload",
      doc: "Extus base dir",
      hidden: true,
      to: "extus.base_dir"
    ],
    "extus.upload_url": [
      commented: false,
      datatype: :binary,
      default: "/files",
      doc: "Extus files url",
      hidden: true,
      to: "extus.upload_url"
    ],
    "extus.expired_after": [
      commented: false,
      datatype: :integer,
      default: 86400000,
      doc: "Extus time before expiration",
      hidden: true,
      to: "extus.expired_after"
    ],
    "extus.clean_interval": [
      commented: false,
      datatype: :integer,
      default: 1800000,
      doc: "Extus clean interval",
      hidden: true,
      to: "extus.clean_interval"
    ],
    "pickweaver.ecto_repos": [
      commented: false,
      datatype: [
        list: :atom
      ],
      default: [
        Pickweaver.Repo
      ],
      doc: "Ecto repos",
      hidden: true,
      to: "pickweaver.ecto_repos"
    ],
    "pickweaver.Elixir.PickweaverWeb.Endpoint.secret_key_base": [
      commented: false,
      datatype: :binary,
      default: "i_am_not_secure_replace_me",
      doc: "Secret key for cookies and various tokens",
      hidden: false,
      to: "pickweaver.Elixir.PickweaverWeb.Endpoint.secret_key_base"
    ],
    "pickweaver.Elixir.PickweaverWeb.Endpoint.render_errors.view": [
      commented: false,
      datatype: :atom,
      default: PickweaverWeb.ErrorView,
      doc: "Errors view",
      hidden: true,
      to: "pickweaver.Elixir.PickweaverWeb.Endpoint.render_errors.view"
    ],
    "pickweaver.Elixir.PickweaverWeb.Endpoint.render_errors.accepts": [
      commented: false,
      datatype: [
        list: :binary
      ],
      default: [
        "html",
        "json"
      ],
      doc: "Errors display formats",
      hidden: true,
      to: "pickweaver.Elixir.PickweaverWeb.Endpoint.render_errors.accepts"
    ],
    "pickweaver.Elixir.PickweaverWeb.Endpoint.pubsub.name": [
      commented: false,
      datatype: :atom,
      default: Pickweaver.PubSub,
      doc: "PubSub name",
      hidden: true,
      to: "pickweaver.Elixir.PickweaverWeb.Endpoint.pubsub.name"
    ],
    "pickweaver.Elixir.PickweaverWeb.Endpoint.pubsub.adapter": [
      commented: false,
      datatype: :atom,
      default: Phoenix.PubSub.PG2,
      doc: "PubSub adapter",
      hidden: true,
      to: "pickweaver.Elixir.PickweaverWeb.Endpoint.pubsub.adapter"
    ],
    "pickweaver.Elixir.PickweaverWeb.Endpoint.debug_errors": [
      commented: false,
      datatype: :atom,
      default: true,
      doc: "Whether to debug errors",
      hidden: true,
      to: "pickweaver.Elixir.PickweaverWeb.Endpoint.debug_errors"
    ],
    "pickweaver.Elixir.PickweaverWeb.Endpoint.code_reloader": [
      commented: false,
      datatype: :atom,
      default: true,
      doc: "Whether to enable code reloader",
      hidden: true,
      to: "pickweaver.Elixir.PickweaverWeb.Endpoint.code_reloader"
    ],
    "pickweaver.Elixir.PickweaverWeb.Endpoint.check_origin": [
      commented: false,
      datatype: :atom,
      default: false,
      doc: "Whether to check origin on websockets",
      hidden: true,
      to: "pickweaver.Elixir.PickweaverWeb.Endpoint.check_origin"
    ],
    "pickweaver.Elixir.PickweaverWeb.Endpoint.live_reload.patterns": [
      commented: false,
      datatype: [
        list: :binary
      ],
      default: [
        ~r/priv\/static\/.*(js|css|png|jpeg|jpg|gif|svg)$/,
        ~r/priv\/gettext\/.*(po)$/,
        ~r/lib\/pickweaver_web\/views\/.*(ex)$/,
        ~r/lib\/pickweaver_web\/templates\/.*(eex)$/
      ],
      doc: "Live reload patterns",
      hidden: true,
      to: "pickweaver.Elixir.PickweaverWeb.Endpoint.live_reload.patterns"
    ],
    "pickweaver.Elixir.Pickweaver.Repo.adapter": [
      commented: false,
      datatype: :atom,
      default: Ecto.Adapters.Postgres,
      doc: "Ecto adapter",
      hidden: true,
      to: "pickweaver.Elixir.Pickweaver.Repo.adapter"
    ],
    "pickweaver.Elixir.PickweaverWeb.Guardian.issuer": [
      commented: false,
      datatype: :binary,
      default: "pickweaver",
      doc: "Guardian token issuer",
      hidden: true,
      to: "pickweaver.Elixir.PickweaverWeb.Guardian.issuer"
    ],
    "pickweaver.Elixir.PickweaverWeb.Guardian.secret_key": [
      commented: false,
      datatype: :binary,
      default: "0Gmquox+i_am_really_not_secure_edit_me_please",
      doc: "Secret key for authentification",
      hidden: false,
      to: "pickweaver.Elixir.PickweaverWeb.Guardian.secret_key"
    ],
    "pickweaver.phoenix_swagger.swagger_files": [
      commented: false,
      datatype: :binary,
      doc: "Define swagger files",
      hidden: true,
      to: "pickweaver.phoenix_swagger.swagger_files"
    ]
  ],
  transforms: [],
  validators: []
]
