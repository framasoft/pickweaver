use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :pickweaver, PickweaverWeb.Endpoint,
  http: [port: 4001],
  server: false,
  instance: "framastory.org",
  email_from: "test@local.tld",
  reply_to: "reply@local.tld"

# Print only warnings and errors during test
config :logger, level: :warn

config :pickweaver, Pickweaver.Mailer,
       adapter: Bamboo.TestAdapter

# Configure your database
config :pickweaver, Pickweaver.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: if(System.get_env("CI"), do: "postgres", else: "elixir"),
  password: if(System.get_env("CI"), do: "", else: "elixir"),
  database: "pickweaver_test",
  hostname: if(System.get_env("CI"), do: "postgres", else: "localhost"),
  pool: Ecto.Adapters.SQL.Sandbox

config :plug, :validate_header_keys_during_test, false

config :exvcr, [
  vcr_cassette_library_dir: "test/fixtures/vcr_cassettes",
]
