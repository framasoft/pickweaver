# Pickweaver

Pickweaver is a Storify clone written with Elixir and the Phoenix Framework for the back-end, and VueJS for the front-end.

It aims to replace Storify when the services closes.
It has the following features :

* Creating and editing "stories" from elements such as 
    * Text elements :
    * Twitter tweets ;
    * YouTube videos ;
    * or any pages with metadata information.

* Importing content from the following sources
    * Storify stories
    * Storify profiles
    * Twitter threads (tweets and replies)
    * Twitter Moments

# Installing and contributing

See the [wiki](https://framagit.org/framasoft/pickweaver/wikis/home).

Feel free to contribute to the wiki.

# License

Pickweaver is licensed under the GNU Affero General Public License v3 license.
